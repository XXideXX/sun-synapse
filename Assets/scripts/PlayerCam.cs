using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCam : MonoBehaviour
{
    [SerializeField] Transform lookat;


    private Vector3 disarePosition;
    [SerializeField] private float offset  = 1.5f;
    [SerializeField] private float distance = 3.5f;
    

    private void Update()
    {
         disarePosition = lookat.position + (-transform.forward * distance) + (transform.up * offset *Time.deltaTime);
          transform.position = Vector3.Lerp(transform.position, disarePosition, 0.08f);
          transform.LookAt(lookat.position + Vector3.up * offset);
        
    }
   
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeteorGenerationField : MonoBehaviour
{
    [SerializeField] private Transform[] asteroidpref;
    public int asteroidCount;


    Vector3Int[] directions =
    {
        new  Vector3Int (0,0,0),
        new  Vector3Int (0,0,1),
        new  Vector3Int (0,1,0),
        new  Vector3Int (0,0,0),
        new  Vector3Int (0,0,-1),
        new  Vector3Int (0,-1,0),
        new  Vector3Int (-1,0,0),
        new  Vector3Int (-1,0,0),
    };
    Vector3Int[] posx =
    {   

        new  Vector3Int (1,0,1),
        new  Vector3Int (1,0,0),
        new  Vector3Int (1,1,0),
        new  Vector3Int (1,1,1),


        new  Vector3Int (1,1,-1),
        new  Vector3Int (1,-1,1),
        new  Vector3Int (1,-1,-1),

        new  Vector3Int (1,0,-1),
        new  Vector3Int (1,-1,0),

    };
    Vector3Int[] posz =
  {

        new  Vector3Int (0,0,1),
        new  Vector3Int (0,1,1),
        new  Vector3Int (1,0,1),
        new  Vector3Int (1,1,1),


        new  Vector3Int (-1,1,1),
        new  Vector3Int (1,-1,1),
        new  Vector3Int (-1,-1,1),

        new  Vector3Int (-1,0,1),
        new  Vector3Int (0,-1,1),

    };
    Vector3Int[] posy =
   {
   
        new  Vector3Int (0,1,0),
        new  Vector3Int (1,1,0),
        new  Vector3Int (0,1,1),
        new  Vector3Int (1,1,1),
        new  Vector3Int (0,1,-1),
        new  Vector3Int (-1,1,0),
        new  Vector3Int (-1,1,1),
        new  Vector3Int (1,1,-1),
        new  Vector3Int (-1,1,-1),

     
  

    };
    Vector3Int[] pouglam =  //20
    {
        //12 �� ������ ���� ���������)
        new  Vector3Int (1,0,1),
        new  Vector3Int (1,1,0),
        new  Vector3Int (0,1,1),



        new  Vector3Int (-1,0,-1),
        new  Vector3Int (0,-1,-1),
        new  Vector3Int (-1,-1,0),


       //������� 8
        new  Vector3Int (1,1,-1),
        new  Vector3Int (1,-1,1),
        new  Vector3Int (-1,1,1),
        new  Vector3Int (-1,-1,1),
        new  Vector3Int (-1,1,-1),
        new  Vector3Int (1,-1,-1),
        new  Vector3Int (1,1,1),
        new  Vector3Int (-1,-1,-1)
    };

    Vector3 spawnmcube;
     Vector3 position;
    List<Vector3Int> gdeuzebil;
    public Vector3 spuwnradius;
    float granicapox;
    float granicapoy;
    float granicapoz;
    float granicapominusx;
    float granicapominusy;
    float granicapominusz;
    private void Start()
    {
        gdeuzebil = new List<Vector3Int>();
        granicapox = spuwnradius.x / 2;
        granicapoy = spuwnradius.y / 2;
        granicapoz = spuwnradius.z / 2;
        granicapominusx = -spuwnradius.x / 2;
        granicapominusy = -spuwnradius.y / 2;
        granicapominusz = -spuwnradius.z / 2;
        spawnmcube = new Vector3(spuwnradius.x, spuwnradius.y, spuwnradius.z);
        position -= spawnmcube / 2;
        foreach (var item in directions)
        {
            GenerateMeteors(item);
        }
        foreach (var item in pouglam)
        {
            GenerateMeteors(item);
        }
    }


    private void Update()
    {
        if (transform.position.x > granicapox)
        {
            granicapox += spuwnradius.x;
          
            position +=  new Vector3 (spuwnradius.x,0,0);
            foreach (var item in posx)
            {
                GenerateMeteors(item);
            }
            
        }
        else if (transform.position.y > granicapoy)
        {
            granicapoy += spuwnradius.y;
            position += new Vector3(0, spuwnradius.y, 0);
            foreach (var item in posy)
            {
                GenerateMeteors(item);
            }
        }
        else if (transform.position.z > granicapoz)
        {
            granicapoz += spuwnradius.z;
            position += new Vector3(0, 0, spuwnradius.z);
            foreach (var item in posz)
            {
                GenerateMeteors(item);
            }
        }
        else if (transform.position.x < granicapominusx)
        {
            granicapominusx += -spuwnradius.x;

            position -= new Vector3(spuwnradius.x, 0, 0);
            foreach (var item in posx)
            {
                GenerateMeteors(-item);
            }
        }
        else if (transform.position.y < granicapominusy)
        {
            granicapominusy += -spuwnradius.y;
   
            position -= new Vector3(0, spuwnradius.y, 0);
            foreach (var item in posy)
            {
                GenerateMeteors(-item);
            }
        }
        else if (transform.position.z < granicapominusz)
        {
            granicapominusz += -spuwnradius.z;
           
            position -= new Vector3(0, 0, spuwnradius.z);
            foreach (var item in posz)
            {
                GenerateMeteors(-item);
            }
        }
       
    }
  


    public void GenerateMeteors(Vector3 multiplayer)
    {

        var chtoto = position + new Vector3 (spawnmcube.x * multiplayer.x, spawnmcube.y * multiplayer.y, spawnmcube.z * multiplayer.z);
      
        for (int i = 0; i < asteroidCount; i++)
        {
            var  position = chtoto + new  Vector3(Random.value * spuwnradius.x,
            Random.value * spuwnradius.y, Random.value * spuwnradius.z);

            Instantiate(asteroidpref[Random.Range(0, asteroidpref.Length)],
            position, Quaternion.identity);
        }
    }
    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawCube(transform.position, spuwnradius);
    }
}

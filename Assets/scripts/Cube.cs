using MFlight;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Cube : MonoBehaviour
{ 
    [SerializeField] private float speedMultyplayer;
    private RaycastHit hit;
    private BoxCollider boxCollider;
    bool m_HitDetect;
    public float roll = 0.0f;
    public float pitch = 0.0f;
    public float yaw = 0.0f;

    public bool commit = false;
    [SerializeField] private MouseFlightController hud;
    [SerializeField] private Slider slider;
    [SerializeField] private Text text;


    private void Start()
    {
        boxCollider = GetComponent<BoxCollider>();
    }

    private void Update()
    {
        var  speed = speedMultyplayer * slider.value;

        text.text = "speed " +((int)(slider.value*100)).ToString();
        m_HitDetect = Physics.BoxCast(transform.position, boxCollider.size, transform.forward , out hit, transform.rotation , 1f); // vmeto 1 chtoto svazanoe so skorostiu i tazmeron box collidera nado sdelat
                                                                                                                                  //(transform.position, boxCollider.size, transform.forward, out hit , Mathf.Abs(1 * Time.deltaTime), LayerMask.GetMask("Player", "Meteors"));// scorost v vectore 3
        if (!m_HitDetect)
        {
           transform.Translate(Vector3.forward * speed * Time.deltaTime);
        }
      //    Vector3 mousePos = Input.mousePosition;

      //    pitch = (mousePos.y - (Screen.height * 0.5f)) / (Screen.height * 0.5f);
      //    yaw = (mousePos.x - (Screen.width * 0.5f)) / (Screen.width * 0.5f);


       //   pitch = -Mathf.Clamp(pitch, -1.0f, 1.0f);
         // yaw = Mathf.Clamp(yaw, -1.0f, 1.0f);

        var localFlyTarget = transform.InverseTransformPoint(hud.MouseAimPos).normalized;


        yaw = Mathf.Clamp(localFlyTarget.x, -1f, 1f);
       pitch = -Mathf.Clamp(localFlyTarget.y, -1f, 1f);

        transform.Rotate(new Vector3(pitch,  yaw, 0));

   
    }


}



